# Individual Project 2
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/individual-project-2/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/individual-project-2/-/commits/main)

## Video
[YouTube](https://www.youtube.com/watch?v=50ICrqOo4Pg)

## Purpose of Project
This project implements continuous delivery of a Rust microservice. This microservice is designed to execute queries related to questions about vegetables rich in vitamin C against a database, utilizing the large language model tool called Cohere for the search process. The obtained results are then visualized, displaying the names of the vegetables along with their descriptions. To facilitate this functionality, a straightforward REST API powered by Cohere API is employed. Additionally, the Docker image containing the microservice is stored within the Elastic Container Registry.

## Requirements
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

## Simple REST API--Result from Query
This query extracts two vegetables and their decriptions that contains vitamin c.
![Screenshot_2024-03-23_at_10.12.17_PM](/uploads/c164e845d833857591509513242559a4/Screenshot_2024-03-23_at_10.12.17_PM.png)

##  Docker Image in AWS
![image](/uploads/03f7375d1de1f22f85e847b7eb124fc9/image.png)

## Lambda
![Screenshot_2024-03-23_at_10.22.30_PM](/uploads/b75d03b919951cd61bd23830699f77e0/Screenshot_2024-03-23_at_10.22.30_PM.png)

## CI/CD pipeline
![Screenshot_2024-03-24_at_8.58.07_PM](/uploads/14a2948268f83b7f47f4752b44ca3ba0/Screenshot_2024-03-24_at_8.58.07_PM.png)